﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace KinectHandTracking
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow  : Window
    {

        Joint handRight;
        Joint handLeft;

        //communication UDP
        public Boolean done = false;
        public Boolean exception_thrown = false;
        Socket sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
        ProtocolType.Udp);
        public IPEndPoint sending_end_point;
        public IPAddress send_to_address;


        #region Members

        public KinectSensor _sensor;
        MultiSourceFrameReader _reader;
        IList<Body> _bodies;

        GestureXML gxml;
        MetaphorCore mtcRight, mtcLeft, mtcRightLast, mtcLeftLast;

      

        int start = 0;
        int startLeft = 0;


        #endregion

        #region Constructor

        public MainWindow()
        {
            //udp
            send_to_address = IPAddress.Parse("127.0.0.1");
            sending_end_point = new IPEndPoint(send_to_address, 11000);

            InitializeComponent();
            gxml = new GestureXML();

            mtcRight = new MetaphorCore();
            mtcLeft  = new MetaphorCore();

            mtcRightLast = new MetaphorCore();
            mtcLeftLast = new MetaphorCore();

            mtcLeftLast.Estado = "NotTracked";
            mtcRightLast.Estado = "NotTracked";

           

        }

        #endregion

        #region Event handlers

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _sensor = KinectSensor.GetDefault();

            if (_sensor != null)
            {
                _sensor.Open();

                _reader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
                _reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (_reader != null)
            {
                _reader.Dispose();
            }

            if (_sensor != null)
            {
                _sensor.Close();
            }
        }

        void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var reference = e.FrameReference.AcquireFrame();

            // Color
            using (var frame = reference.ColorFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    camera.Source = frame.ToBitmap();
                }
            }

            // Body
            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    canvas.Children.Clear();

                    _bodies = new Body[frame.BodyFrameSource.BodyCount];

                    frame.GetAndRefreshBodyData(_bodies);

                    foreach (var body in _bodies)
                    {
                        if (body != null)
                        {
                            if (body.IsTracked)
                            {
                                // Find the joints
                                handRight = body.Joints[JointType.HandRight];
                                Joint thumbRight = body.Joints[JointType.ThumbRight];

                                handLeft = body.Joints[JointType.HandLeft];
                                Joint thumbLeft = body.Joints[JointType.ThumbLeft];

                                // Draw hands and thumbs
                                canvas.DrawHand(handRight, _sensor.CoordinateMapper);
                                canvas.DrawHand(handLeft, _sensor.CoordinateMapper);
                                canvas.DrawThumb(thumbRight, _sensor.CoordinateMapper);
                                canvas.DrawThumb(thumbLeft, _sensor.CoordinateMapper);    

                                switch (body.HandRightState)
                                {
                                    
                                    case HandState.Open:
                                        if (start == 0)
                                        {
                                            mtcRightLast.Estado = "Open";
                                            start++;
                                        }
                                        if (!gxml.CheckLastState(HandState.Open, mtcRightLast.Estado))
                                        {
                                            AcaoMaoDireitaRealizadaTexto.Text = mtcRightLast.Estado;        
                                            mtcRight = gxml.MethaphorContext(HandState.Open,"PRINCIPAL");
                                            mtcRight.Comando = gxml.MethaphorContextAction(mtcRight, mtcRightLast);
                                        }
                                        break;
                                    case HandState.Closed:
                                        if (start == 0)
                                        {
                                            mtcRightLast.Estado = "Closed";
                                            start++;
                                        }
                                        if (!gxml.CheckLastState(HandState.Closed, mtcRightLast.Estado))
                                        {
                                            mtcRight = gxml.MethaphorContext(HandState.Closed, "PRINCIPAL");
                                            mtcRight.Comando = gxml.MethaphorContextAction(mtcRight, mtcRightLast);
                                        }
                                        break;
                                    case HandState.Lasso:
                                        if (start == 0)
                                        {
                                            mtcRightLast.Estado = "Lasso";
                                            start++;
                                        }
                                        if (!gxml.CheckLastState(HandState.Lasso, mtcRightLast.Estado))
                                       {
                                            mtcRight = gxml.MethaphorContext(HandState.Lasso, "PRINCIPAL");
                                            mtcRight.Comando = gxml.MethaphorContextAction(mtcRight, mtcRightLast);
                                       }
                                        break;
                                        /*
                                    case HandState.Unknown:
                                        if (start == 0)
                                        {
                                            mtcRightLast.State = "Unknown";
                                            start++;
                                        }
                                        if (!gxml.CheckLastState(HandState.Unknown, mtcRightLast.State))
                                        {
                                            mtcRight = gxml.MethaphorContext(HandState.Unknown);
                                            x = gxml.MethaphorContextAction(mtcRight, mtcRightLast);

                                        }
                                        break;
                                    case HandState.NotTracked:
                                        if (start == 0)
                                        {
                                            mtcRightLast.State = "NotTracked";
                                            start++;
                                        }
                                        if (!gxml.CheckLastState(HandState.NotTracked, mtcRightLast.State))
                                       {
                                            mtcRight = gxml.MethaphorContext(HandState.NotTracked);
                                            x = gxml.MethaphorContextAction(mtcRight, mtcRightLast);
                                       }
                                        break;
                                   */ 
                                    default:
                                        break;
                                    
                                }

                                switch (body.HandLeftState)
                                {
                                    
                                    case HandState.Open:
                                        if (startLeft == 0)
                                        {
                                            mtcLeftLast.Estado = "Open";
                                            startLeft++;
                                        }
                                        mtcLeft = gxml.MethaphorContext(HandState.Open, "SECUNDARIA");
                                        if (mtcRight.Estado.Equals("Closed"))
                                        {
                                            mtcLeft.Comando = "Rotacionar";
                                        }
                                        if (mtcRight.Estado.Equals("Open"))
                                        {
                                            mtcLeft.Comando = "Reset";
                                        }
                                        


                                        break;
                                    case HandState.Closed:
                                        if (startLeft == 0)
                                        {
                                            mtcLeftLast.Estado = "Closed";
                                            startLeft++;

                                        }
                                        mtcLeft = gxml.MethaphorContext(HandState.Closed, "SECUNDARIA");
                                        if (mtcRight.Estado.Equals("Closed"))
                                        {
                                            mtcLeft.Comando = "Redimensionar";
                                           
                                        }
                                        
                                        break;
                                    case HandState.Lasso:
                                        if (startLeft == 0)
                                        {
                                            mtcLeftLast.Estado = "Lasso";
                                            startLeft++;
                                        }
                                        mtcLeft = gxml.MethaphorContext(HandState.Lasso, "SECUNDARIA");
                                        if (mtcRight.Estado.Equals("Lasso")){
                                            mtcLeft.Comando = "Navegar";
                                        }
                                        if (mtcRight.Estado.Equals("Closed"))
                                        {
                                            mtcLeft.Comando = "Deslocar";
                                        }
                                        if (mtcRight.Estado.Equals("Open"))
                                        {
                                            mtcLeft.Comando = "Navegar";
                                        }

                                        break;
                                        /*
                                    case HandState.Unknown:
                                        mtcLeft = gxml.MethaphorContext(HandState.Unknown);
                                        break;
                                    case HandState.NotTracked:
                                        mtcLeft = gxml.MethaphorContext(HandState.NotTracked);
                                        break;
                                        */
                                    default:
                                        break;
                                }

                                //Direita
                                tbl_MD_Metafora.Text = mtcRight.Metafora;
                                tbl_MD_Estado.Text = mtcRight.Estado;
                                tbl_MD_Interacao.Text = mtcRight.Interacao;
                                tbl_MD_Acao.Text = mtcRight.Acao;

                                //Esquerda
                                tbl_ME_Metafora.Text = mtcLeft.Metafora;
                                tbl_ME_Estado.Text = mtcLeft.Estado;
                                tbl_ME_Interacao.Text = mtcLeft.Interacao;
                                tbl_ME_Acao.Text = mtcLeft.Acao;

                                tbl_MD_Estado_DuasMaos.Text = mtcLeft.Comando;
                                //tbl_MD_Acao_Copy.Text = mtcRightLast.State;
                                AcaoMaoDireitaRealizadaTexto.Text = mtcRight.Comando;

                                //string text_to_send = tbl_MD_Estado_DuasMaos.Text+ ";"+Extensions.xX.ToString()+";"+Extensions.yY.ToString();
                                /*string text_to_send = 
                                    tbl_MD_Estado_DuasMaos.Text + ";" 
                                    +handLeft.Position.X.ToString() +";" + handLeft.Position.Y.ToString()+";"+handLeft.Position.Z.ToString()+";"
                                    +handRight.Position.X.ToString()+";"+handRight.Position.Y.ToString()+";"+handRight.Position.Z.ToString();
                                    */
                                string text_to_send =
                                    tbl_MD_Estado_DuasMaos.Text + ";"
                                    + handRight.Position.X.ToString() + ";" + handRight.Position.Y.ToString() + ";" + handRight.Position.Z.ToString() +";" 
                                    + handLeft.Position.X.ToString() + ";" + handLeft.Position.Y.ToString() + ";" + handLeft.Position.Z.ToString()+";"+ AcaoMaoDireitaRealizadaTexto.Text + ";" + tbl_MD_Estado_DuasMaos.Text;
                                
                                byte[] send_buffer = Encoding.ASCII.GetBytes(text_to_send);
                                sending_socket.SendTo(send_buffer, sending_end_point);

                            }
                        }
                    }
                }
            }
        }

        #endregion
    }

}
