﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectHandTracking
{
    public class MetaphorCore
    {
        String metafora = "--";
        String acao = "--";
        String estado = "--";
        String interacao = "--";
        String stateLast = "--";
        String comando = "-----";

        public string Metafora
        {
            get
            {
                return metafora;
            }

            set
            {
                metafora = value;
            }
        }

        public string Acao
        {
            get
            {
                return acao;
            }

            set
            {
                acao = value;
            }
        }

        public string Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public string Interacao
        {
            get
            {
                return interacao;
            }

            set
            {
                interacao = value;
            }
        }

        public string StateLast { get => stateLast; set => stateLast = value; }
        public string Comando { get => comando; set => comando = value; }
    }
}
