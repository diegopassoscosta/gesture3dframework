﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Kinect;
using System.Xml.Linq;

namespace KinectHandTracking
{
    public class GestureXML
    {
        MetaphorCore mtc;

        public GestureXML()
        {
        }
        public MetaphorCore MethaphorContext(HandState SensorState, string SelecaoDaMao)
        {
            mtc = new MetaphorCore();

            String state = SensorState.ToString();

            if (SelecaoDaMao.Equals("PRINCIPAL"))
            {
                XDocument xdocConsulta = XDocument.Load("Gesture.xml");

                xdocConsulta.Descendants("MaoPrincipal").Where(p => p.Element
                   ("Estado").Value.Equals(state.ToString())).Select(p => new
                   {
                       metafora = p.Element("Metafora").Value,
                       interacao = p.Element("Interacao").Value,
                       acao = p.Element("Acao").Value,
                       estado = p.Element("Estado").Value

                   }).ToList().ForEach(p => {
                       mtc.Metafora = p.metafora;
                       mtc.Interacao = p.interacao;
                       mtc.Acao = p.acao;
                       mtc.Estado = p.estado;

                   });

            }


            if (SelecaoDaMao.Equals("SECUNDARIA"))
            {
                XDocument xdocConsulta = XDocument.Load("Gesture.xml");

                xdocConsulta.Descendants("MaoSecundaria").Where(p => p.Element
                   ("Estado").Value.Equals(state.ToString())).Select(p => new
                   {
                       metafora = p.Element("Metafora").Value,
                       interacao = p.Element("Interacao").Value,
                       acao = p.Element("Acao").Value,
                       estado = p.Element("Estado").Value

                   }).ToList().ForEach(p => {
                       mtc.Metafora = p.metafora;
                       mtc.Interacao = p.interacao;
                       mtc.Acao = p.acao;
                       mtc.Estado = p.estado;

                   });

            }




            return mtc;
        }

        public String MethaphorContextAction(MetaphorCore mtcAtual, MetaphorCore mtcAnterior)
        {

            if (mtcAtual.Estado.Equals("Lasso"))
            {


                if (mtcAnterior.Estado.Equals("Closed"))
                {
                    mtcAnterior.Estado = "Lasso";
                    return "Apontar Objetos";

                }

                if (mtcAnterior.Estado.Equals("Open"))
                {
                    mtcAnterior.Estado = "Lasso";
                    return "Apontar Objetos";

                }
                /*
                if (mtcAnterior.State.Equals("NotTracked"))
                {
                    mtcAnterior.State = "Lasso";
                    return "Apontar";

                }

                if (mtcAnterior.State.Equals("Unknown"))
                {
                    mtcAnterior.State = "Lasso";
                    return "Apontar";

                }
                */

            }

            if (mtcAtual.Estado.Equals("Closed"))
            {

                if (mtcAnterior.Estado.Equals("Lasso"))
                {
                    mtcAnterior.Estado = "Closed";
                    return "Seleção por RayCasting";

                }


                if (mtcAnterior.Estado.Equals("Open"))
                {
                    mtcAnterior.Estado = "Closed";
                    return "Seleção por VirtualHand";
                }
                /*
                if (mtcAnterior.State.Equals("NotTracked"))
                {
                    mtcAnterior.State = "Closed";
                    return "Reset";

                }

                if (mtcAnterior.State.Equals("Unknown"))
                {
                    mtcAnterior.State = "Closed";
                    return "Reset";

                }
                */

            }

            if (mtcAtual.Estado.Equals("Open"))
            {
                if (mtcAnterior.Estado.Equals("Closed"))
                {
                    mtcAnterior.Estado = "Open";
                    return "Tocar Objetos";

                }

                if (mtcAnterior.Estado.Equals("Lasso"))
                {
                    mtcAnterior.Estado = "Open";
                    return "Tocar Objetos";

                }
                /*
                if (mtcAnterior.State.Equals("NotTracked"))
                {
                    mtcAnterior.State = "Open";
                    return "Mantenha - VirtualHand";

                }

                if (mtcAnterior.State.Equals("Unknown"))
                {
                    mtcAnterior.State = "Open";
                    return "UTOPEN";

                }
                */

            }




            if (mtcAtual.Estado.Equals("NotTracked"))
            {

                if (mtcAnterior.Estado.Equals("Lasso"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Closed"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Open"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Unknown"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }


            }

            if (mtcAtual.Estado.Equals("Unknown"))
            {

                if (mtcAnterior.Estado.Equals("Lasso"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Closed"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Open"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("NotTracked"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }
                


            }

            return "Ação da mão principal não rastreada, não indentificada";
        }

        public String MethaphorContextCommand(MetaphorCore mtcAtualPrincipal, MetaphorCore mtcAtual, MetaphorCore mtcAnterior)
        {

            if (mtcAtualPrincipal.Estado.Equals("Lasso"))
            {
                if (mtcAtual.Estado.Equals("Lasso"))
                {

                    if (mtcAnterior.Estado.Equals("Closed"))
                    {
                        mtcAnterior.Estado = "Lasso";
                        return "Navegar";

                    }

                    if (mtcAnterior.Estado.Equals("Open"))
                    {
                        mtcAnterior.Estado = "Lasso";
                        return "Navegar";

                    }
                    /*
                    if (mtcAnterior.State.Equals("NotTracked"))
                    {
                        mtcAnterior.State = "Lasso";
                        return "Apontar";

                    }

                    if (mtcAnterior.State.Equals("Unknown"))
                    {
                        mtcAnterior.State = "Lasso";
                        return "Apontar";

                    }
                    */

                }

            }

            /*Mais complexo até o momento*/
            if (mtcAtualPrincipal.Estado.Equals("Closed"))
            {

                if (mtcAtual.Estado.Equals("Lasso"))
                {

                    if (mtcAnterior.Estado.Equals("Lasso"))
                    {
                        mtcAnterior.Estado = "Lasso";
                        return "Manipular - Deslocar X/Y";

                    }


                    if (mtcAnterior.Estado.Equals("Closed"))
                    {
                        mtcAnterior.Estado = "Lasso";
                        return "Manipular - Deslocar X/Y";

                    }


                    if (mtcAnterior.Estado.Equals("Open"))
                    {
                        mtcAnterior.Estado = "Lasso";
                        return "Manipular - Deslocar X/Y";
                    }
                    /*
                    if (mtcAnterior.State.Equals("NotTracked"))
                    {
                        mtcAnterior.State = "Closed";
                        return "Reset";

                    }

                    if (mtcAnterior.State.Equals("Unknown"))
                    {
                        mtcAnterior.State = "Closed";
                        return "Reset";

                    }
                    */

                }

                if (mtcAtual.Estado.Equals("Closed"))
                {

                    if (mtcAnterior.Estado.Equals("Closed"))
                    {
                        mtcAnterior.Estado = "Closed";
                        return "Manipular -Redimensionar X/Y";
                    }

                    if (mtcAnterior.Estado.Equals("Lasso"))
                    {
                        mtcAnterior.Estado = "Closed";
                        return "Manipular -Redimensionar X/Y";

                    }
                    

                    if (mtcAnterior.Estado.Equals("Open"))
                    {
                        mtcAnterior.Estado = "Closed";
                        return "Manipular -Redimensionar X/Y";
                    }
                    /*
                    if (mtcAnterior.State.Equals("NotTracked"))
                    {
                        mtcAnterior.State = "Closed";
                        return "Reset";

                    }

                    if (mtcAnterior.State.Equals("Unknown"))
                    {
                        mtcAnterior.State = "Closed";
                        return "Reset";

                    }
                    */

                }

                if (mtcAtual.Estado.Equals("Open"))
                {

                    if (mtcAnterior.Estado.Equals("Open"))
                    {
                        mtcAnterior.Estado = "Open";
                        return "Manipular - Rotacionar X/Y";

                    }


                    if (mtcAnterior.Estado.Equals("Closed"))
                    {
                        mtcAnterior.Estado = "Open";
                        return "Manipular - Rotacionar X/Y";

                    }


                    if (mtcAnterior.Estado.Equals("Lasso"))
                    {
                        mtcAnterior.Estado = "Open";
                        return "Manipular - Rotacionar X/Y";
                    }
                    /*
                    if (mtcAnterior.State.Equals("NotTracked"))
                    {
                        mtcAnterior.State = "Closed";
                        return "Reset";

                    }

                    if (mtcAnterior.State.Equals("Unknown"))
                    {
                        mtcAnterior.State = "Closed";
                        return "Reset";

                    }
                    */

                }


            }


            if (mtcAtualPrincipal.Estado.Equals("Open"))
            {
                if (mtcAtual.Estado.Equals("Lasso"))
                {

                    if (mtcAnterior.Estado.Equals("Closed"))
                    {
                        mtcAnterior.Estado = "Lasso";
                        return "Navegar";
                    }

                   if (mtcAnterior.Estado.Equals("Closed"))
                    {
                        mtcAnterior.Estado = "Lasso";
                        return "Navegar";
                    }

                    if (mtcAnterior.Estado.Equals("Lasso"))
                     {
                          mtcAnterior.Estado = "Lasso";
                          return "Navegar";
                     }
                    /*
                    if (mtcAnterior.State.Equals("NotTracked"))
                    {
                        mtcAnterior.State = "Open";
                        return "Mantenha - VirtualHand";

                    }

                    if (mtcAnterior.State.Equals("Unknown"))
                    {
                        mtcAnterior.State = "Open";
                        return "UTOPEN";

                    }
                    */
                }
            }
            







            if (mtcAtual.Estado.Equals("NotTracked"))
            {

                if (mtcAnterior.Estado.Equals("Lasso"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Closed"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Open"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Unknown"))
                {
                    mtcAnterior.Estado = "NotTracked";
                    return "----";

                }


            }

            if (mtcAtual.Estado.Equals("Unknown"))
            {

                if (mtcAnterior.Estado.Equals("Lasso"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Closed"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("Open"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }

                if (mtcAnterior.Estado.Equals("NotTracked"))
                {
                    mtcAnterior.Estado = "Unknown";
                    return "----";

                }



            }

            return "Ação da mão principal não rastreada, não indentificada";
        }

        internal bool CheckLastState(HandState SensorState, string state)
        {
            return SensorState.ToString().Equals(state.ToString());

        }
    }
}



