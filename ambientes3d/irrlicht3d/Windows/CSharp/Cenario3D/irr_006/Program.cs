﻿using System;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace irr_006
{

    class Program
    {
        public static string[] comando = { "none", "-270", "230" };
        //antes da gravação TOP01
        //public static string[] comando = {"none","50","50"};
        public static String nomeObj = "";
        public static int xCam = 0;
 

        
        public static float xDesc = 10.0f;


        //udp sinc
        public const int listenPort = 11000;
        public static bool done = false;
        public static UdpClient listener = new UdpClient(listenPort);
        public static IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);
     

        //udp async
        private static Socket udpSock;
        private static byte[] buffer;

        //manupular camera
        public static int numerolixo = 50;
        public static int numerolixoTexto = 0;


        const int ID_IsNotPickable = 0;
        const int IDFlag_IsPickable = 1 << 0;
        const int IDFlag_IsHighlightable = 1 << 1;

        private static void DoReceiveFrom(IAsyncResult iar)
        {
            try
            {
                //Get the received message.
                Socket recvSock = (Socket)iar.AsyncState;
                EndPoint clientEP = new IPEndPoint(IPAddress.Any, 0);
                int msgLen = recvSock.EndReceiveFrom(iar, ref clientEP);
                byte[] localMsg = new byte[msgLen];
                Array.Copy(buffer, localMsg, msgLen);

                //Start listening for a new message.
                EndPoint newClientEP = new IPEndPoint(IPAddress.Any, 0);
                udpSock.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref newClientEP, DoReceiveFrom, udpSock);

                //Handle the received message
                Console.WriteLine("Recieved {0} bytes from {1}:{2}",
                                  msgLen,
                                  ((IPEndPoint)clientEP).Address,
                                  ((IPEndPoint)clientEP).Port);
                //Do other, more interesting, things with the received message.
            }
            catch (ObjectDisposedException)
            {
                //expected termination exception on a closed socket.
                // ...I'm open to suggestions on a better way of doing this.
            }
        }

        static void Main(string[] args)
        {
            udpSock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpSock.Bind(new IPEndPoint(IPAddress.Any, 12345));
            buffer = new byte[1024];



            try
            {
                //TOP00
                DriverType driverType;
                if (!AskUserForDriver(out driverType))
                    return;

                IrrlichtDevice device = IrrlichtDevice.CreateDevice(driverType, new Dimension2Di(1024, 768));
                if (device == null)
                    return;

                VideoDriver driver = device.VideoDriver;
                SceneManager smgr = device.SceneManager;
                

                device.FileSystem.AddFileArchive("media/map-20kdm2.pk3");

                AnimatedMesh q3levelmesh = smgr.GetMesh("20kdm2.bsp");
                MeshSceneNode q3node = null;

                // The Quake mesh is pickable, but doesn't get highlighted.
                if (q3levelmesh != null)
                    q3node = smgr.AddOctreeSceneNode(q3levelmesh.GetMesh(0), null, IDFlag_IsPickable);

                TriangleSelector selector = null;

                if (q3node != null)
                {
                    q3node.Position = new Vector3Df(-1350, -130, -1400);
                    selector = smgr.CreateOctreeTriangleSelector(q3node.Mesh, q3node, 128);
                    q3node.TriangleSelector = selector;
                    // We're not done with this selector yet, so don't drop it.
                }

                // Set a jump speed of 3 units per second, which gives a fairly realistic jump
                // when used with the gravity of (0, -10, 0) in the collision response animator.
                CameraSceneNode camera = smgr.AddCameraSceneNodeFPS(null, 100.0f, 0.3f, ID_IsNotPickable, null, true, 3.0f);
                camera.Position = new Vector3Df(100, 50, -60);
                camera.Target = new Vector3Df(-700, 30, -60);

                if (selector != null)
                {
                    SceneNodeAnimator anim = smgr.CreateCollisionResponseAnimator(
                        selector, camera,
                        new Vector3Df(30, 50, 30),
                        new Vector3Df(0, -10, 0),
                        new Vector3Df(0, 30, 0));

                    selector.Drop(); // As soon as we're done with the selector, drop it.
                    camera.AddAnimator(anim);
                    anim.Drop(); // And likewise, drop the animator when we're done referring to it.
                }
                
                // Now I create three animated characters which we can pick, a dynamic light for
                // lighting them, and a billboard for drawing where we found an intersection.

                // First, let's get rid of the mouse cursor. We'll use a billboard to show what we're looking at.
                device.CursorControl.Visible = false;

                // Add the billboard.
                BillboardSceneNode bill = smgr.AddBillboardSceneNode();
                bill.SetMaterialType(MaterialType.TransparentAddColor);
                bill.SetMaterialTexture(0, driver.GetTexture("media/particle.bmp"));
                bill.SetMaterialFlag(MaterialFlag.Lighting, false);
                bill.SetMaterialFlag(MaterialFlag.ZBuffer, false);
                bill.SetSize(20, 20, 20);
                bill.ID = ID_IsNotPickable; // This ensures that we don't accidentally ray-pick it

                AnimatedMeshSceneNode node = null;

                

                // This X files uses skeletal animation, but without skinning.
                
                node = smgr.AddAnimatedMeshSceneNode(smgr.GetMesh("media/dwarf.x"), null, IDFlag_IsPickable | IDFlag_IsHighlightable);
                node.Position = new Vector3Df(-90, -66, -60); // Put its feet on the floor.
                node.Rotation = new Vector3Df(0, -90, 0); // And turn it towards the camera.
                node.AnimationSpeed = 20.0f;
                node.Name = "Anao";
                selector = smgr.CreateTriangleSelector(node);
                node.TriangleSelector = selector;
                node.Visible = false;


                selector.Drop();
                


                // Now create a triangle selector for it.  The selector will know that it
                // is associated with an animated node, and will update itself as necessary.
                selector = smgr.CreateTriangleSelector(node);
                node.TriangleSelector = selector;
                selector.Drop(); // We're done with this selector, so drop it now.

                // And this mdl file uses skinned skeletal animation.
                node = smgr.AddAnimatedMeshSceneNode(smgr.GetMesh("media/yodan.mdl"), null, IDFlag_IsPickable | IDFlag_IsHighlightable);
                //200 node.Position = new Vector3Df(100, -45, 10);
                //node.Position = new Vector3Df(-90, -40, -100);

                
                //antes da gravacao
                node.Position = new Vector3Df(200, -40, 10);

                //na gravacao
                node.Position = new Vector3Df(-90, -25, 20);


                //node.Position = new Vector3Df(-90, -40, -0);
                //node.Rotation = new Vector3Df(0, 90, 0); 

                node.Scale = new Vector3Df(0.8f);
                node.GetMaterial(0).Lighting = true;
                node.AnimationSpeed = 0.0f;
                node.Name = "yodan";

                // Just do the same as we did above.
                selector = smgr.CreateTriangleSelector(node);
                node.TriangleSelector = selector;
                selector.Drop();

                // Add a light, so that the unselected nodes aren't completely dark.
                LightSceneNode light = smgr.AddLightSceneNode(null, new Vector3Df(-60, 100, 400), new Colorf(1.0f, 1.0f, 1.0f), 600.0f);
                light.ID = ID_IsNotPickable; // Make it an invalid target for selection.

                // Remember which scene node is highlighted
                SceneNode highlightedSceneNode = null;
                SceneCollisionManager collMan = smgr.SceneCollisionManager;
                int lastFPS = -1;

                // draw the selection triangle only as wireframe
                Material material = new Material();
                material.Lighting = false;
                material.Wireframe = true;

                while (device.Run())
                    if (device.WindowActive)
                    {
                        driver.BeginScene(ClearBufferFlag.All, new Color(0));
                        smgr.DrawAll();

                        // Unlight any currently highlighted scene node
                        if (highlightedSceneNode != null)
                        {
                            highlightedSceneNode.SetMaterialFlag(MaterialFlag.Lighting, true);
                            highlightedSceneNode = null;
                        }

                        // All intersections in this example are done with a ray cast out from the camera to
                        // a distance of 1000.  You can easily modify this to check (e.g.) a bullet
                        // trajectory or a sword's position, or create a ray from a mouse click position using
                        // ISceneCollisionManager::getRayFromScreenCoordinates()
                        Line3Df ray = new Line3Df();
                        ray.Start = camera.Position;
                        ray.End = ray.Start + (camera.Target - ray.Start).Normalize() * 1000.0f;

                        

                        listener.BeginReceive(new AsyncCallback(recv), null);
                        
                        //antes da gravação
                        camera.Rotation = new Vector3Df(float.Parse(comando[1])*200f, float.Parse(comando[2])*200f, 90);
                        //camera.Rotation = new Vector3Df(float.Parse(comando[1]), float.Parse(comando[2]), 90);
                       

                        // Tracks the current intersection point with the level or a mesh
                        Vector3Df intersection;
                        // Used to show with triangle has been hit
                        Triangle3Df hitTriangle;

                        // This call is all you need to perform ray/triangle collision on every scene node
                        // that has a triangle selector, including the Quake level mesh.  It finds the nearest
                        // collision point/triangle, and returns the scene node containing that point.
                        // Irrlicht provides other types of selection, including ray/triangle selector,
                        // ray/box and ellipse/triangle selector, plus associated helpers.
                        // See the methods of ISceneCollisionManager
                        SceneNode selectedSceneNode =
                            collMan.GetSceneNodeAndCollisionPointFromRay(
                                ray,
                                out intersection, // This will be the position of the collision
                                out hitTriangle, // This will be the triangle hit in the collision
                                IDFlag_IsPickable); // This ensures that only nodes that we have set up to be pickable are considered

                        // If the ray hit anything, move the billboard to the collision position
                        // and draw the triangle that was hit.
                        if (selectedSceneNode != null )
                        {
                            bill.Position = intersection;

                            if (comando[0].Equals("Navegar"))
                            {
                                //TOP4
                                //ANTES DA GRAVAÇÃO
                                //camera.Position = ray.End
                                camera.Position = ray.End - 200;
                                camera.Position.Normalize();
                                camera.Target = ray.End;
                                //camera.Position = ray.End * 1.0001F;


                                //camera.Position.Normalize();
                            }
                            // We need to reset the transform before doing our own rendering.
                            driver.SetTransform(TransformationState.World, Matrix.Identity);
                            driver.SetMaterial(material);
                            driver.Draw3DTriangle(hitTriangle, new Color(255, 0, 0));
                            if (comando[0].Equals("Reset"))
                            {
                                smgr.GetSceneNodeFromName("yodan").Position = new Vector3Df(200, -40, 10);
                                node.Rotation = new Vector3Df(0, 180, 0);

                                node.Scale = new Vector3Df(0.8f);


                                //camera.Position = new Vector3Df(50, 50, -60);
                            }
                            // We can check the flags for the scene node that was hit to see if it should be
                            // highlighted. The animated nodes can be highlighted, but not the Quake level mesh
                            if ((selectedSceneNode.ID & IDFlag_IsHighlightable) == IDFlag_IsHighlightable && selectedSceneNode.Name.Equals("yodan"))
                            {
                                xDesc = float.Parse(comando[4]);
                                if ("Selecao por RayCasting".Equals("Selecao por RayCasting"))
                                {
                                    bill.SetMaterialTexture(0, driver.GetTexture("media/particle.bmp"));
                                    if (comando[0].Equals("Redimensionar"))
                                    {
                                        VideoDriver drive2 = device.VideoDriver;

                                       
                                        if (xDesc > -0.3f)
                                        {
                                            smgr.GetSceneNodeFromName("yodan").Scale *= new Vector3Df(1.0f * 1.0001F);
                                            //      Console.WriteLine("Maior");
                                            //smgr.GetSceneNodeFromName("yodan").Scale.RotateXYby(10.0f);// = 10.0f;// smgr.GetSceneNodeFromName("yodan").Scale.X + 1.0f;
                                            //smgr.GetSceneNodeFromName("yodan").Scale /= new Vector3Df(smgr.GetSceneNodeFromName("yodan").Scale.X-0.000001f, smgr.GetSceneNodeFromName("yodan").Scale.Y - 0.000001f, smgr.GetSceneNodeFromName("yodan").Scale.Z - 0.000001f).Normalize();
                                            //smgr.GetSceneNodeFromName("yodan").
                                        }
                                        else
                                        {
                                            smgr.GetSceneNodeFromName("yodan").Scale *= new Vector3Df(1.0f / 1.0001F);
                                            //      Console.WriteLine("Menor");
                                            //     smgr.GetSceneNodeFromName("yodan").Scale = new Vector3Df(0.9f).Normalize();
                                        }
                                        //smgr.GetSceneNodeFromName("yodan").Scale = new Vector3Df(float.Parse(comando[4]) * -100f, float.Parse(comando[5]) * 100f, 40);

                                    }

                                    if (comando[0].Equals("Rotacionar"))
                                    {
                                        //TOPSEMIFINAL
                                        //float x, y, z;
                                        //x = float.Parse(comando[4]) * -100f;
                                        //y = 180;
                                        //z = 40;
                                        if (xDesc > -0.3f)
                                        {
                                            smgr.GetSceneNodeFromName("yodan").Rotation *= new Vector3Df(1.0f / 1.0001F);
                                        }
                                        else
                                        {
                                            smgr.GetSceneNodeFromName("yodan").Rotation *= new Vector3Df(1.0f * 1.0001F);
                                        }
                                            //smgr.GetSceneNodeFromName("yodan").Rotation = new Vector3Df(x * -100f, 180, 40);
                                            smgr.GetSceneNodeFromName("yodan").Scale = new Vector3Df(0.8f);
                                    }

                                    if (comando[0].Equals("Deslocar"))
                                    {
                                        //float xDesc = 0.0f;
                                        xDesc = float.Parse(comando[4]);

                                        if (xDesc > -0.3f)
                                            smgr.GetSceneNodeFromName("yodan").Position *= new Vector3Df(1.0f * 1.0001F);
                                        else
                                          smgr.GetSceneNodeFromName("yodan").Position *= new Vector3Df(1.0f / 1.0001F);
                                        //TOP3
                                        //ANTES DA GRAVAÇÃO
                                        //smgr.GetSceneNodeFromName("yodan").Position = new Vector3Df(250, -10, 10);  // new Vector3Df(0, 90, 0); new Vector3Df(float.Parse(comando[4]) * 100f, float.Parse(comando[5]) * -100f, 10);
                                        //smgr.GetSceneNodeFromName("yodan").Position.Normalize();
                                        //smgr.GetSceneNodeFromName("yodan").Position = new Vector3Df(smgr.GetSceneNodeFromName("yodan").Position.X + 0.00001f,
                                        //    smgr.GetSceneNodeFromName("yodan").Position.Y , smgr.GetSceneNodeFromName("yodan").Position.Z );
                                        //smgr.GetSceneNodeFromName("yodan").Position = new Vector3Df(float.Parse(comando[4]) * 100f, float.Parse(comando[5]) * -100f, float.Parse(comando[6]) * 100f);
                                        smgr.GetSceneNodeFromName("yodan").Scale = new Vector3Df(0.8f);
                                    }



                                }
                                //se seleção por raycasting
                                //ver comando da segunda mao
                                //rotationar
                                //scalar
                                //rotacionar
                                highlightedSceneNode = selectedSceneNode;

                                // Highlighting in this case means turning lighting OFF for this node,
                                // which means that it will be drawn with full brightness.
                                highlightedSceneNode.SetMaterialFlag(MaterialFlag.Lighting, false);

                                //if (ContexModel.Action.Escale == RemoteContextModel.Action) {





                                //}

                                //if (smgr.GetSceneNodeFromName("Anao") != null)
                                // {
                                //    nomeObj = highlightedSceneNode.Name.ToString();
                                //smgr.GetSceneNodeFromName("Anao").Visible = false;
                                //   smgr.GetSceneNodeFromName("Anao");
                                //}
                            }
                            else {
                               // bill.SetMaterialTexture(0, driver.GetTexture("C:/Users/Diego/OneDrive/MestradoTOPFINAL/irr_006funcional_novo/irr_006/bin/Debug/vh0.png"));
                            }
                        }

                        // We're all done drawing, so end the scene.
                        driver.EndScene();

                        int fps = driver.FPS;
                        if (lastFPS != fps)
                        {
                            device.SetWindowCaption(String.Format(
                                "Irrlicht [{0}] fps: {1} Rotation X: {2} Rotation Y: {3} Rotation Z: {4} NomeObj: {5}",
                                driver.Name, fps, camera.Rotation.X, camera.Rotation.Y, camera.Rotation.Z, xDesc));

                            lastFPS = fps;
                        }
                    }

                device.Drop();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }

        private static void recv(IAsyncResult res)
        {
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 8000);
            byte[] received = listener.EndReceive(res, ref RemoteIpEndPoint);

            //Process codes

            
            String mensagem = Encoding.UTF8.GetString(received);
            Console.WriteLine(mensagem);
            comando = mensagem.Split(';');
            //xCam = Int32.Parse(x);

            listener.BeginReceive(new AsyncCallback(recv), null);
        }

        private static void UDPListener()
        {
            Task.Run(async () =>
            {
                using (var udpClient = new UdpClient(11000))
                {
                    string loggingEvent = "";
                    while (true)
                    {
                        //IPEndPoint object will allow us to read datagrams sent from any source.
                        var receivedResults = await udpClient.ReceiveAsync();
                        loggingEvent += Encoding.ASCII.GetString(receivedResults.Buffer);
                        Console.WriteLine("ola " + loggingEvent);
                    }
                }
            });
        }


        static bool AskUserForDriver(out DriverType driverType)
        {
            driverType = DriverType.Null;

            Console.Write("Please select the driver you want for this example:\n" +
                        " (a) OpenGL\n (b) Direct3D 9.0c\n" +
                        " (c) Burning's Software Renderer\n (d) Software Renderer\n" +
                        " (e) NullDevice\n (otherKey) exit\n\n");

            ConsoleKeyInfo i = Console.ReadKey();

            switch (i.Key)
            {
                case ConsoleKey.A: driverType = DriverType.OpenGL; break;
                case ConsoleKey.B: driverType = DriverType.Direct3D9; break;
                case ConsoleKey.C: driverType = DriverType.BurningsVideo; break;
                case ConsoleKey.D: driverType = DriverType.Software; break;
                case ConsoleKey.E: driverType = DriverType.Null; break;
                default:
                    return false;
            }

            return true;
        }

        

       



    }

}
